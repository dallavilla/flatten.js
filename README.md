# Flatten.js
Flatten.js is a jQuery plugin that stabs affordance in the face. Tired of garish
gradients and harsh box-shadows? :wave: goodbye with flatten.js

## Usage
First, include jQuery and Flatten.js in your document. Feel free to throw it in your ```<head>```
tag. Don't worry about performance, you'll make it up with all the skeuo styles you'll eradicate.
```html
<script src="jquery.min.js" type="text/javascript"></script>
<script src="jquery.flatten.js" type="text/javascript"></script>
```

Then...
```javascript
$().flatten()
```

Boom.
