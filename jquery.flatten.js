/*
 * Flatten.js: a jQuery plugin, version: 1.0 (2013-06-04)
 * @requires jQuery v1.7 or later
 *
 * Flatten.js is a jQuery plugin that stabs affordance in the face.
 */
;(function( $ ) {
  $.fn.flatten = function() {
    var $els = $('*'), $elsWithGradients

    $elsWithGradients = $($.grep($els, function(el) {
      return /gradient/.test($(el).css('background-image'))
    }))

    $elsWithGradients.each(function(){
      $(this).css({
        'background-image': 'none'
      })
    })

    $els.each(function() {
      $(this).css({
        'border': 'none',
        'box-shadow': 'none',
        'text-shadow': 'none'
      })
    })
  }
}(window.jQuery))
